require('should')
const jsreport = require('../')
const path = require('path')

describe('all extensions', function () {
  let reporter
  beforeEach(() => {
    reporter = jsreport({
      rootDirectory: path.join(__dirname, '../'),
      loadConfig: false,
    })
    return reporter.init()
  })
  afterEach(() => reporter.close())
  it('()=>should have template', async () => {
    jsreport.render({
      template: { name: 'invoice-main' },
      data: {
        "number": "123",
        "seller": {
          "name": "Next Step Webs, Inc.",
          "road": "12345 Sunny Road",
          "country": "Sunnyville, TX 12345"
        },
        "buyer": {
          "name": "Acme Corp.",
          "road": "16 Johnson Road",
          "country": "Paris, France 8060"
        },
        "items": [{
          "name": "Website design",
          "price": 300
        }]
      }
    }).then((resp) => {
      console.log(resp)
    }).catch((e) => {
      console.error(e);
    });
  })
})
